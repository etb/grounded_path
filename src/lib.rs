use std::ffi::OsStr;
use std::path::{Path, PathBuf};

#[derive(Debug, PartialEq, Clone)]
pub struct GroundedPath {
    root_path: PathBuf,
    local_path: PathBuf,
}

impl GroundedPath {
    pub fn new(root_path: &Path) -> Self {
        GroundedPath {
            root_path: PathBuf::from(root_path),
            local_path: PathBuf::from("/"),
        }
    }

    pub fn from_local(root_path: &Path, local_path: &Path) -> Self {
        Self::new(root_path).join(local_path)
    }

    pub fn from_system(root_path: &Path, system_path: &Path) -> Result<Self, ()> {
        match system_path.strip_prefix(root_path.clone()) {
            Ok(path) => Ok(Self::new(root_path).join(path)),
            Err(_e) => Err(()),
        }
    }

    pub fn root_path(&self) -> &Path {
        self.root_path.as_path()
    }

    pub fn local_path(&self) -> &Path {
        self.local_path.as_path()
    }

    pub fn system_path(&self) -> PathBuf {
        let local_path: PathBuf = self
            .local_path
            .iter()
            .fold(PathBuf::new(), |mut pb, i| {
                let root = OsStr::new("/");
                let dot = OsStr::new(".");
                let ddot = OsStr::new("..");
                if i == dot {
                } else if i == ddot {
                    if pb != PathBuf::from(root) {
                        pb.pop();
                    }
                } else {
                    pb.push(i);
                }
                pb
            })
            .iter()
            .filter(|i| i != &OsStr::new(&std::path::MAIN_SEPARATOR.to_string()))
            .collect();
        self.root_path.join(local_path)
    }

    pub fn join(self, path: &Path) -> Self {
        GroundedPath {
            root_path: self.root_path,
            local_path: self.local_path.join(path),
        }
    }

    pub fn chroot(self, new_root: &Path) -> Self {
        GroundedPath {
            root_path: new_root.to_path_buf(),
            local_path: self.local_path,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn new() {
        let path = Path::new("/some/path");
        let grounded = GroundedPath::new(path);

        assert_eq!(grounded.root_path(), path);
        assert_eq!(grounded.local_path(), Path::new("/"));
        assert_eq!(grounded.system_path(), path);
    }

    #[test]
    pub fn from_system() {
        let base_path = Path::new("/base/path");
        let valid_sub_path = Path::new("/base/path/some/valid/path");
        let invalid_sub_path = Path::new("/base/some/invalid/path");
        assert!(GroundedPath::from_system(base_path, valid_sub_path).is_ok());
        assert!(!GroundedPath::from_system(base_path, invalid_sub_path).is_ok());
        assert_eq!(
            GroundedPath::from_system(base_path, valid_sub_path)
                .unwrap()
                .root_path(),
            Path::new("/base/path")
        );
        assert_eq!(
            GroundedPath::from_system(base_path, valid_sub_path)
                .unwrap()
                .local_path(),
            Path::new("/some/valid/path")
        );
    }

    #[test]
    pub fn from_local() {
        let base_path = Path::new("/base/path");
        let absolute_local_path = Path::new("/abs/local/path");
        let relative_local_path = Path::new("rel/local/path");

        let grounded = GroundedPath::from_local(base_path, absolute_local_path);
        assert_eq!(grounded.root_path(), base_path);
        assert_eq!(grounded.local_path(), absolute_local_path);
        assert_eq!(
            grounded.system_path(),
            Path::new("/base/path/abs/local/path")
        );

        let grounded = GroundedPath::from_local(base_path, relative_local_path);
        assert_eq!(grounded.root_path(), base_path);
        assert_eq!(
            grounded.local_path(),
            Path::new("/").join(relative_local_path)
        );
        assert_eq!(
            grounded.system_path(),
            Path::new("/base/path/rel/local/path")
        );
    }

    #[test]
    pub fn system_path() {
        let base_path = Path::new("/base/path");
        let local_path = Path::new("/../../../local/../path");

        let grounded = GroundedPath::from_local(base_path, local_path);
        assert_eq!(grounded.root_path(), base_path);
        assert_eq!(grounded.local_path(), local_path);
        // never go over the root_path
        assert_eq!(grounded.system_path(), Path::new("/base/path/path"));
    }

    #[test]
    pub fn chroot() {
        let old_base_path = Path::new("/old/base/path");
        let new_base_path = Path::new("/new/base/path");
        let local_path = Path::new("/local/path");

        let grounded = GroundedPath::from_local(old_base_path, local_path);
        assert_eq!(grounded.root_path(), old_base_path);
        let grounded = grounded.chroot(new_base_path);
        assert_eq!(grounded.root_path(), new_base_path);
    }

    #[test]
    pub fn join() {
        let base_path = Path::new("/base/path");
        let local_path = Path::new("/local/path");

        let grounded = GroundedPath::from_local(base_path, local_path);
        assert_eq!(grounded.root_path(), base_path);
        assert_eq!(grounded.local_path(), local_path);
        let grounded = grounded.join(Path::new("added/path"));
        assert_eq!(grounded.root_path(), base_path);
        assert_eq!(grounded.local_path(), local_path.join("added/path"));
    }
}
